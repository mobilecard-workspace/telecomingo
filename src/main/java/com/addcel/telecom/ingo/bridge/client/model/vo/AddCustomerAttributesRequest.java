package com.addcel.telecom.ingo.bridge.client.model.vo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddCustomerAttributesRequest {
	
	//private List<SessionAttribute> customerAttributes = new ArrayList<SessionAttribute>();
	private List<Attributes> customerAttributes = new ArrayList<Attributes>();

	public AddCustomerAttributesRequest() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the customerAttributes
	 */
	public List<Attributes> getCustomerAttributes() {
		return customerAttributes;
	}

	/**
	 * @param customerAttributes the customerAttributes to set
	 */
	public void setCustomerAttributes(List<Attributes> customerAttributes) {
		this.customerAttributes = customerAttributes;
	}
	
	
	

}
