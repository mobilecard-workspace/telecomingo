package com.addcel.telecom.ingo.telecomingo.mybatis.model.vo;

public class DataUser {

	private String nusuario;
	private String nclave;
	private String respuesta;
	
	/**
	 * @return the nusuario
	 */
	public String getNusuario() {
		return nusuario;
	}
	/**
	 * @param nusuario the nusuario to set
	 */
	public void setNusuario(String nusuario) {
		this.nusuario = nusuario;
	}
	/**
	 * @return the nclave
	 */
	public String getNclave() {
		return nclave;
	}
	/**
	 * @param nclave the nclave to set
	 */
	public void setNclave(String nclave) {
		this.nclave = nclave;
	}
	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}
	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	
	
}
