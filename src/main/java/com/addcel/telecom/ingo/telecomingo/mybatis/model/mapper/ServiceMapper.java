package com.addcel.telecom.ingo.telecomingo.mybatis.model.mapper;

import java.util.HashMap;

import org.apache.ibatis.annotations.Param;

import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.BitacoraIngo;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.BlacksPayData;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.ClienteVO;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.DataUser;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.Estados;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.MCCard;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.Proveedor;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.User;





public interface ServiceMapper {

	//public ClienteVO getClient(@Param(value = "user") String user, @Param(value = "pass") String pass);
	public User getCustumerIDbyId(@Param(value = "user") long  user);
	public User getCustumerID(@Param(value = "user") String user, @Param(value = "pass") String pass);
	public Integer TelecomCard(long id_usuario);
	public String  getUser(HashMap datauser);
	public void updateCustomerId(User user);
	public void insertBitacoraIngo(BitacoraIngo bitacoraIngo);
	public BlacksPayData getPayBlackData(String id_ingo);
	public Proveedor getProveedor(String proveedor);
	public MCCard getTelecomCard(long id_usuario);
	public Estados getState(@Param(value = "abb") String abb, @Param(value = "id") int id);
	
}
