package com.addcel.telecom.ingo.telecomingo.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

//import com.addcel.wallet.services.UtilsService;
import com.addcel.telecom.ingo.bridge.client.model.vo.AddOrUpdateCardRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.EnrollCustomerMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.FindCustomerRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.IngoNotifications;
import com.addcel.telecom.ingo.bridge.client.model.vo.LoginRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.SessionResponse;
import com.addcel.telecom.ingo.telecomingo.services.MCIngoServices;
import com.addcel.utils.AddcelCrypto;

@Controller
public class RestServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestServices.class);
	
	@Autowired
	private MCIngoServices IngoServices;
	
	
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public ModelAndView getToken() {
		
		ModelAndView mav = new ModelAndView("index");
		return mav;
	}
	
	@RequestMapping(value = "/403", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<CustomerResponse> error403() {
		/*String password = "Mc1nGo$17uSa";
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		LOGGER.debug("PASSSSS: *************** " + hashedPassword);
		System.out.println("PASSSSS: *************** " + hashedPassword);*/
		String password = "ANDROID";
		LOGGER.debug("PASSSSS: *************** " + AddcelCrypto.encryptHard("ANDROID"));
		CustomerResponse response = new CustomerResponse();
		
		
		return  new ResponseEntity<CustomerResponse>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getSessionData", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<SessionResponse> getSessionData(@RequestBody LoginRequest s) {
		return new ResponseEntity<SessionResponse>(IngoServices.getSessionData(s),HttpStatus.OK);
	}

	@RequestMapping(value = "/IngoNotifications", method = RequestMethod.POST,produces = "application/json")
	public  ResponseEntity<String> Notifications(@RequestBody IngoNotifications notificactions){
		LOGGER.debug("Recibiendo notificación");
		LOGGER.debug("CustomerId "+ notificactions.getCustomerId());
		LOGGER.debug("CustomerEmailAddress "+ notificactions.getCustomerEmailAddress());
		LOGGER.debug("FirstName "+ notificactions.getFirstName());
		LOGGER.debug("CancelledByDescription "+ notificactions.getCancelledByDescription());
		LOGGER.debug("CardBinNumber "+ notificactions.getCardBinNumber());
		LOGGER.debug("CardId "+ notificactions.getCardId());
		LOGGER.debug("CardNickName "+ notificactions.getCardNickName());
		LOGGER.debug("CheckSubmissionDate "+ notificactions.getCheckSubmissionDate());
		LOGGER.debug("CustomerEmailAddress "+ notificactions.getCustomerEmailAddress());
		LOGGER.debug("DeclineCode "+ notificactions.getDeclineCode());
		LOGGER.debug("DeclineReason "+ notificactions.getDeclineReason());
		LOGGER.debug("FundingDestinationIdentifier "+ notificactions.getFundingDestinationIdentifier());
		LOGGER.debug("FundsAvailableDate "+ notificactions.getFundsAvailableDate());
		LOGGER.debug("LastFourOfCard "+ notificactions.getLastFourOfCard());
		LOGGER.debug("LastName "+ notificactions.getLastName());
		LOGGER.debug("NotificationId "+ notificactions.getNotificationId());
		LOGGER.debug("NotificationTypeDescription "+ notificactions.getNotificationTypeDescription());
		LOGGER.debug("PromoCode "+ notificactions.getPromoCode());
		LOGGER.debug("PromoType "+ notificactions.getPromoType());
		LOGGER.debug("TransactionId "+ notificactions.getTransactionId());
		LOGGER.debug("TransactionStatusDescription "+ notificactions.getTransactionStatusDescription());
		LOGGER.debug("TransactionTypeDescription "+ notificactions.getTransactionTypeDescription());
		LOGGER.debug("CancelledBy "+ notificactions.getCancelledBy());
		LOGGER.debug("Fee "+ notificactions.getFee());
		LOGGER.debug("FeeDeltaInCents "+ notificactions.getFeeDeltaInCents());
		LOGGER.debug("IsPpg "+ notificactions.getIsPpg());
		LOGGER.debug("KeyedAmount "+ notificactions.getKeyedAmount());
		LOGGER.debug("LoadAmount "+ notificactions.getLoadAmount());
		LOGGER.debug("NotificationType "+ notificactions.getNotificationType());
		LOGGER.debug("PromoAmount "+ notificactions.getPromoAmount());
		LOGGER.debug("TransactionStatus "+ notificactions.getTransactionStatus());
		LOGGER.debug("TransactionType "+ notificactions.getTransactionType());
		//System.out.println(notificactions.getFirstName());
		//System.out.println(notificactions.getCustomerId());
		//@RequestBody RequestNotifications notificactions
		
		IngoServices.NotificationsSave(notificactions);
		
		return new ResponseEntity<String>("OK",HttpStatus.OK);
	}
	
	@RequestMapping(value = "/EnrollCustomer", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CustomerResponse> EnrollCustomer(@RequestBody EnrollCustomerMC enrollMC){
		LOGGER.debug("Iniciando proceso de enrolamiento");
		CustomerResponse response = IngoServices.EnrollCustomer(enrollMC);
		enrollMC.setCustomerId(response.getCustomerId());
		CardsResponse cardresponse = null;
		/*if(response.getErrorCode() == 0)
		{
			AddOrUpdateCardRequestMC requestMC = new AddOrUpdateCardRequestMC();
			requestMC.setAddressLine1(enrollMC.getAddressLine1());
			requestMC.setAddressLine2(enrollMC.getAddressLine2());
			requestMC.setCardNickname(enrollMC.getCardNickname());
			requestMC.setCardNumber(enrollMC.getCardNumber());
			requestMC.setCity(enrollMC.getCity());
			requestMC.setCustomerId(enrollMC.getCustomerId());
			requestMC.setDiviceId("123456789");
			requestMC.setExpirationMonthYear(enrollMC.getExpirationMonthYear());
			requestMC.setNameOnCard(enrollMC.getNameOnCard());
			requestMC.setState(enrollMC.getState());
			requestMC.setZip(enrollMC.getZip());
			
			cardresponse = IngoServices.AddOrUpdateCard(requestMC);
		}
		
		if (cardresponse == null || cardresponse.getErrorCode() != 0)
		{
			response.setErrorCode(cardresponse.getErrorCode());
			response.setErrorMessage(cardresponse.getErrorMessage());
		}*/
		return  new ResponseEntity<CustomerResponse>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/AddOrUpdateCard",method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CardsResponse>  AddOrUpdateCard(@RequestBody AddOrUpdateCardRequestMC addOrUpdateRequest){
		CardsResponse response = IngoServices.AddOrUpdateCard(addOrUpdateRequest);
		return  new ResponseEntity<CardsResponse>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value="/FindCustomer", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CustomerResponse> FindCustomer(@RequestBody FindCustomerRequestMC findcustomer){
		
		CustomerResponse response = IngoServices.FindCustomer(findcustomer);
		return  new ResponseEntity<CustomerResponse>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value="/GetRegisteredCards", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CardsResponse> GetRegisteredCards(@RequestBody GetRegisteredCardsRequestMC request){
		CardsResponse response = IngoServices.GetRegisteredCards(request);
		return  new ResponseEntity<CardsResponse>(response,HttpStatus.OK);
	}

}
