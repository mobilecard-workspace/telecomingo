package com.addcel.telecom.ingo.telecomingo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.telecom.ingo.bridge.client.ServicesEndPoint;
import com.addcel.telecom.ingo.bridge.client.model.vo.AddOrUpdateCardRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticateOBORequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticateOBOResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticatePartnerResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.BlackstoneRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.BlackstoneResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.EnrollCustomerRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.FindCustomerRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.SessionRequest;
import com.google.gson.Gson;

@Service
public class MCIngorequest {
	
	@Autowired
	private RestTemplate restTemplate;
	
	
public AuthenticatePartnerResponse getSession(SessionRequest session, String deviceId, String plataforma ){
		
		try{
			
			HttpHeaders headers = getHeader(deviceId, null);
			headers.add("plataforma",plataforma == null || plataforma.isEmpty() ? "ANDROID" : plataforma);
			HttpEntity<SessionRequest> request = new HttpEntity<SessionRequest>(session,headers);

			AuthenticatePartnerResponse responseObject  =  (AuthenticatePartnerResponse)  restTemplate.postForObject( ServicesEndPoint.getSession, request, AuthenticatePartnerResponse.class);
			return responseObject;
		
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
	}

public AuthenticateOBOResponse AuthenticateOBO (AuthenticateOBORequest authenticateOBORequest, String deviceId){
	try{
		
		
		HttpHeaders headers = getHeader(deviceId, null);
		
		HttpEntity<AuthenticateOBORequest> request = new HttpEntity<AuthenticateOBORequest>(authenticateOBORequest, headers);
		
		AuthenticateOBOResponse responseObject  =  (AuthenticateOBOResponse)  restTemplate.postForObject(ServicesEndPoint.AuthenticateOBO , request, AuthenticateOBOResponse.class);
		return responseObject;
		
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public CustomerResponse EnrollCustomer(EnrollCustomerRequest enrollCustomerRequest, String deviceId){
	
try{
		
		
		HttpHeaders headers = getHeader(deviceId, null);
		
		HttpEntity<EnrollCustomerRequest> request = new HttpEntity<EnrollCustomerRequest>(enrollCustomerRequest, headers);
		
		CustomerResponse responseObject  =  (CustomerResponse)  restTemplate.postForObject(ServicesEndPoint.EnrollCustomer , request, CustomerResponse.class);
		return responseObject;
		
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public CardsResponse AddOrUpdateCard(AddOrUpdateCardRequest addOrUpdateCardRequest, String deviceId){
	
try{
		
		
		HttpHeaders headers = getHeader(deviceId, null);
		
		HttpEntity<AddOrUpdateCardRequest> request = new HttpEntity<AddOrUpdateCardRequest>(addOrUpdateCardRequest, headers);
		
		CardsResponse responseObject  =  (CardsResponse)  restTemplate.postForObject(ServicesEndPoint.AddOrUpdateCard , request, CardsResponse.class);
		return responseObject;
		
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public CustomerResponse FindCustomer(FindCustomerRequest Findrequest, String deviceId){
	try{
		HttpHeaders headers = getHeader(deviceId, null);
		HttpEntity<FindCustomerRequest> request = new HttpEntity<FindCustomerRequest>(Findrequest, headers);
		CustomerResponse responseObject  =  (CustomerResponse)  restTemplate.postForObject(ServicesEndPoint.FindCustomer , request, CustomerResponse.class);
		return responseObject;
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public CardsResponse GetRegisteredCards(GetRegisteredCardsRequest Getrequest, String deviceId){
	try{
		HttpHeaders headers = getHeader(deviceId, null);
		HttpEntity<GetRegisteredCardsRequest> request = new HttpEntity<GetRegisteredCardsRequest>(Getrequest, headers);
		CardsResponse responseObject  =  (CardsResponse)  restTemplate.postForObject(ServicesEndPoint.GetRegisteredCards , request, CardsResponse.class);
		return responseObject;
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
}

public BlackstoneResponse BlackstonePay(BlackstoneRequest Brequest){
	
	try{
		Gson gson = new Gson();		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("json", gson.toJson(Brequest));

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<BlackstoneResponse> response = restTemplate.postForEntity("http://192.168.75.51:80/TelecomBlackstoneServices/telecom/blackstone/send/payment", map,BlackstoneResponse.class);		
		return response.getBody();
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
	
}


private HttpHeaders getHeader(String deviceId, String  sessionId){
	
	try{
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
			
		if(deviceId != null)
			headers.add("deviceId", deviceId);
		
		if(sessionId != null)
			headers.add("sessionId", sessionId);
		
		return headers;
		
	}catch(Exception ex){
		ex.printStackTrace();
		return null;
	}
		
}

}



