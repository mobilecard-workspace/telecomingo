package com.addcel.telecom.ingo.telecomingo.services;

import java.io.UnsupportedEncodingException;
import java.security.acl.NotOwnerException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import com.addcel.telecom.ingo.bridge.client.model.vo.AddOrUpdateCardRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.AddOrUpdateCardRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticateOBORequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticateOBOResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.AuthenticatePartnerResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.BlackstoneRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.BlackstoneResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.Card;
import com.addcel.telecom.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.telecom.ingo.bridge.client.model.vo.EnrollCustomerMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.EnrollCustomerRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.FindCustomerRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.FindCustomerRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.GetRegisteredCardsRequestMC;
import com.addcel.telecom.ingo.bridge.client.model.vo.IngoNotifications;
import com.addcel.telecom.ingo.bridge.client.model.vo.LoginRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.SessionRequest;
import com.addcel.telecom.ingo.bridge.client.model.vo.SessionResponse;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.mapper.ServiceMapper;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.BitacoraIngo;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.BlacksPayData;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.DataUser;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.MCCard;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.Proveedor;
import com.addcel.telecom.ingo.telecomingo.mybatis.model.vo.User;
import com.addcel.telecom.ingo.telecomingo.spring.component.UtilsService;
import com.addcel.telecom.ingo.telecomingo.utils.UtilsMC;
import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;


@Service
public class MCIngoServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(MCIngoServices.class);
	
	@Autowired
	private MCIngorequest mcIngorequest;
	
	@Autowired
	private ServiceMapper mapper;
	
	@Autowired
	private UtilsService jsonUtils;
	
	//sstoken
		//custumer_id
		//session_id
		public SessionResponse getSessionData(LoginRequest login){
			try{
				
				/*String password = "guest";
				PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String hashedPassword = passwordEncoder.encode(password);
				LOGGER.debug("PASSSSS: *************** " + hashedPassword);*/
				SessionResponse response =  new SessionResponse();
				String customerId = null;
				SessionRequest sessionRequest = new SessionRequest();				

				String passdecrypt = AddcelCrypto.decryptHard(login.getPass());	
				String passmx = generaSemillaAux(passdecrypt);
				login.setPass(Crypto.aesEncrypt(passmx, passdecrypt));
				
				User user = null; //mapper.getCustumerID(login.getUser(), login.getPass());
				if(UtilsMC.isNumeric(login.getUser()))
					user = mapper.getCustumerIDbyId(Long.parseLong(login.getUser()));
				else
					user= mapper.getCustumerID(login.getUser(), login.getPass());
				
				int mobilecardCard = mapper.TelecomCard(user.getId_usuario()).intValue();
				
				
				
				
				
				if(mobilecardCard == 1)
				{
					
					CardsResponse  activeIngo = RegisteredCard(user, login.getDeviceId());
					
					if(activeIngo.getErrorCode()== 0 && activeIngo.getCards().size() > 0)
					{
					
							AuthenticatePartnerResponse  authenticatePartnerResponse = mcIngorequest.getSession(sessionRequest, login.getDeviceId(), login.getPlataforma());
							response.setSessionId(authenticatePartnerResponse.getSessionId());
							
							if(response.getSessionId()== null || response.getSessionId().isEmpty())
							{
								response.setErrorCode(authenticatePartnerResponse.getErrorCode());
								response.setErrorMessage(authenticatePartnerResponse.getErrorMessage());
							}
							else
							{
							
								customerId = user.getId_ingo();//mapper.getCustumerID(login.getUser(), login.getPass());//"9f60d6b0-b2ba-4508-978e-0171f1f13f6d";
								response.setCustomerId(customerId);
								
								if(response.getCustomerId() == null || response.getCustomerId().isEmpty())
								{
									response.setErrorCode(100);
									response.setErrorMessage("Error fetching database data: customerId is null or empty" );
								}
								else
								{
									AuthenticateOBORequest authenticateOBORequest = new AuthenticateOBORequest();
									authenticateOBORequest.setCustomerId(customerId);
									AuthenticateOBOResponse  authenticateOBOResponse  = mcIngorequest.AuthenticateOBO(authenticateOBORequest, login.getDeviceId());
									response.setSsoToken(authenticateOBOResponse.getSsoToken());
									response.setErrorCode(authenticateOBOResponse.getErrorCode());
									response.setErrorMessage(authenticateOBOResponse.getErrorMessage());
								}
							}
				  }
					else
					{
						response.setCustomerId("");
						response.setErrorCode(activeIngo.getErrorCode());
						response.setErrorMessage(activeIngo.getErrorMessage());
						response.setSessionId("");
						response.setSsoToken("");
					}
				}
				else
				{
					response.setCustomerId("");
					response.setErrorCode(90);
					response.setErrorMessage("You do not have Telecom card. Do you want to get a Telecom card?");
					response.setSessionId("");
					response.setSsoToken("");
				}
				//EnrollCustomer("infernodidante","Ak0ynTun/y9it4Avvgthdg==");
				//getUser("artcervs","TXgw59dS670mwLAsapjA1w==");
				LOGGER.debug("CustomerId: " + response.getCustomerId());
				LOGGER.debug("SessionId: " + response.getSessionId());
				LOGGER.debug("SsoToken: "+response.getSsoToken());
				LOGGER.debug("ErrorMessage: " + response.getErrorMessage());
				LOGGER.debug("ErrorCode: " + response.getErrorCode());
				return response;
				
			}catch(Exception  ex){
				ex.printStackTrace();
				return null;
			}
		}
		
		/*
		 * Checar si el usuario cuenta con tarjeta enrrola en ingo
		 */
		private CardsResponse RegisteredCard(User user, String deviceId){
			
			LOGGER.info("RegisteredCard");
			CardsResponse response = new CardsResponse();
			//response.setErrorCode(0);
			try{
				if(!user.getId_ingo().isEmpty())
				{					
					GetRegisteredCardsRequest Registercard = new GetRegisteredCardsRequest();
					Registercard.setCustomerId(user.getId_ingo());				
					response = mcIngorequest.GetRegisteredCards(Registercard, deviceId);					
					if(response.getCards() == null || response.getCards().size() == 0){						
						
						String cardNickname = user.getUsr_nombre().substring(0,user.getUsr_nombre().length()> 17 ? 17 : user.getUsr_nombre().length());						
						MCCard card = mapper.getTelecomCard(user.getId_usuario());
						AddOrUpdateCardRequest request = new AddOrUpdateCardRequest();						
						request.setAddressLine1(user.getUsr_direccion());						
						request.setAddressLine2("");						
						request.setCardNickname(cardNickname);						
						request.setCardNumber(UtilsService.getSMS(card.getPan()));						
						request.setCity(user.getUsr_ciudad());						
						request.setCustomerId(user.getId_ingo());						
						request.setExpirationMonthYear(UtilsService.getSMS(card.getVigencia().replace("/", "")));						
						request.setNameOnCard(card.getNombre());						
						request.setState(mapper.getState("", user.getUsr_id_estado().intValue()).getAbreviatura());						
						request.setZip(user.getUsr_cp());						
						//zip,state,nameOnCard,customerId,city,addressLine1
						 response = mcIngorequest.AddOrUpdateCard(request, deviceId);						 
						if(response.getErrorCode() != 0){
							response.setErrorCode(92);
							response.setErrorMessage("Please activate your card by calling: +18558847575");
							response.setCards(new ArrayList<Card>());
						}
						else{
							LOGGER.info("RegisteredCard19");
						}
					}
				}
				else
				{
					response.setErrorCode(91);
					response.setErrorMessage("Not registered to use the service");
					response.setCards(new ArrayList<Card>());
				}
				return response;
			}catch(Exception  ex){
				response.setErrorCode(60);
				response.setErrorMessage("Error Processing Data");
				response.setCards(new ArrayList<Card>());
				return response;
			}
		}
		
		public CustomerResponse EnrollCustomer(EnrollCustomerMC enrollMC){
			
			try{
				
				
				EnrollCustomerRequest request = new EnrollCustomerRequest();
				request.setAddressLine1(enrollMC.getAddressLine1());
				request.setAddressLine2(enrollMC.getAddressLine2());
				request.setAllowTexts(enrollMC.getAllowTexts());
				request.setCity(enrollMC.getCity());
				request.setCountryOfOrigin(enrollMC.getCountryOfOrigin()); //optional 
				request.setDateOfBirth(enrollMC.getDateOfBirth()); //yyyy-MM-dd (e.g., 1978-09-18) 1994-07-02 00:00:00
				request.setEmail(enrollMC.getEmail());
				request.setFirstName(enrollMC.getFirstName());
				request.setGender(enrollMC.getGender()); //Optional Single character string indicating M/F.
				request.setHomeNumber(enrollMC.getHomeNumber());
				request.setLastName(enrollMC.getLastName());
				request.setMiddleInitial(enrollMC.getMiddleInitial()); //Optional Single character middle initial. Max length is 1 character.
				request.setMobileNumber(enrollMC.getMobileNumber());
				request.setSsn(enrollMC.getSsn());
				request.setState(enrollMC.getState());
				request.setSuffix(enrollMC.getSuffix()); // optional Typically would contain one of Jr, Sr, etc.
				request.setTitle(enrollMC.getTitle()); // optional Typically would contain one of Mrs, Mr, etc.
				request.setZip(enrollMC.getZip());
				
				CustomerResponse response =  mcIngorequest.EnrollCustomer(request, enrollMC.getDeviceId());
				LOGGER.debug("json: " + response.getCustomerId() + " " + response.getErrorMessage());
				//actualizar customerid del user;
				/*if(response.getErrorCode().equals(0))
				{
					userObject.setId_ingo(response.getCustomerId());
					//mapper.updateCustomerId(userObject);
				}*/
				
				return response;
				
				//LOGGER.debug("json: " + jsonDataUser);
				
			}catch(Exception ex){
				LOGGER.error("EnrollCustomer error " + ex.getMessage());
				return null;
			}
			
		}
	
		
		public CardsResponse AddOrUpdateCard(AddOrUpdateCardRequestMC requestMC){
			try{				
				LOGGER.debug("Add Card: {}", requestMC);
				AddOrUpdateCardRequest request = new AddOrUpdateCardRequest();
				request.setAddressLine1(requestMC.getAddressLine1());
				request.setAddressLine2(requestMC.getAddressLine2());
				request.setCardNickname(requestMC.getCardNickname());
				request.setCardNumber(requestMC.getCardNumber()); //AddcelCrypto.decryptSensitive(
				request.setCity(requestMC.getCity());
				request.setCustomerId(requestMC.getCustomerId());
				request.setExpirationMonthYear(requestMC.getExpirationMonthYear()); //month/year
				request.setNameOnCard(requestMC.getNameOnCard()); //The name of the user found on the front of the card
				request.setState(requestMC.getState());
				request.setZip(requestMC.getZip());
				
				CardsResponse response = mcIngorequest.AddOrUpdateCard(request, requestMC.getDiviceId());
				return response;
				
			}catch(Exception ex){
				LOGGER.error("Error al Agregar o actualizar Tarjeta " + ex.getMessage());
				return null;
			}
		}
		
		
		public CustomerResponse FindCustomer (FindCustomerRequestMC findcustomer){
			try{
				FindCustomerRequest request = new FindCustomerRequest();
				request.setDateOfBirth(findcustomer.getDateOfBirth());
				request.setSsn(findcustomer.getSsn());
				CustomerResponse response =  mcIngorequest.FindCustomer(request, findcustomer.getDeviceId());
				return response;
				
			}catch(Exception ex){
				LOGGER.error("Se produjo un error al obtener customer " + ex.getMessage());
				return null;
			}
		}
		
		public CardsResponse GetRegisteredCards(GetRegisteredCardsRequestMC requestGet){
			try{				
				GetRegisteredCardsRequest request = new GetRegisteredCardsRequest();				
				request.setCustomerId(requestGet.getCustomerId());
				return mcIngorequest.GetRegisteredCards(request, requestGet.getDeviceId());
			}catch(Exception ex){
				LOGGER.error("Se produjo un error al obtener tarjetas " + ex.getMessage());
				return null;
			}
		}
		
		public void NotificationsSave(IngoNotifications notificactions){
			try{
				
				
				BitacoraIngo bitacora = new BitacoraIngo();
				bitacora.setCancelledBy(notificactions.getCancelledBy() == null ? "" : notificactions.getCancelledBy().toString());
				bitacora.setCancelledByDescription(notificactions.getCancelledByDescription());
				bitacora.setCardBinNumber(notificactions.getCardBinNumber());
				bitacora.setCardId(notificactions.getCardId());
				bitacora.setCardNickName(notificactions.getCardNickName());
				bitacora.setCheckSubmissionDate(notificactions.getCheckSubmissionDate());
				bitacora.setCustomerId(notificactions.getCustomerId());
				bitacora.setDeclineCode(notificactions.getDeclineCode());
				bitacora.setDeclineReason(notificactions.getDeclineReason());
				bitacora.setFee(notificactions.getFee().toString());
				bitacora.setFeeDeltaInCents(notificactions.getFeeDeltaInCents().toString());
				bitacora.setFundingDestinationIdentifier(notificactions.getFundingDestinationIdentifier());
				bitacora.setFundsAvailableDate(notificactions.getFundsAvailableDate());
				bitacora.setIsPpg(notificactions.getIsPpg().toString());
				bitacora.setKeyedAmount(notificactions.getKeyedAmount().toString());
				bitacora.setLastFourOfCard(notificactions.getLastFourOfCard());
				bitacora.setLoadAmount(notificactions.getLoadAmount().toString());
				bitacora.setNotificationId(notificactions.getNotificationId());
				bitacora.setNotificationType(notificactions.getNotificationType().toString());
				bitacora.setNotificationTypeDescription(notificactions.getNotificationTypeDescription());
				bitacora.setPromoAmount(notificactions.getPromoAmount().toString());
				bitacora.setPromoCode(notificactions.getPromoCode());
				bitacora.setPromoType(notificactions.getPromoType());
				bitacora.setTransactionId(notificactions.getTransactionId());
				bitacora.setTransactionStatus(notificactions.getTransactionStatus().toString());
				bitacora.setTransactionStatusDescription(notificactions.getTransactionStatusDescription());
				bitacora.setTransactionType(notificactions.getTransactionType().toString());
				bitacora.setTransactionTypeDescription(notificactions.getTransactionTypeDescription());
				bitacora.setIdbitacora(-1);
				
				
				BlacksPayData data = mapper.getPayBlackData(notificactions.getCustomerId());
				Proveedor proveedor= mapper.getProveedor("INGO");
				
				LOGGER.debug("DAtos recuperados: ");
				
			   long NotificationType = notificactions.getNotificationType();
			   int Transactionstatus =  notificactions.getTransactionStatus();
			   double comisionP = proveedor.getComision_porcentaje();//1;
			   double cobroComision = 0;
			   double cantidad = notificactions.getKeyedAmount()/100d;
			   
			   //Cheque depositado en tarjera mobilecard
			   if(NotificationType == 24 && Transactionstatus == 3){
				   LOGGER.debug("Entrando al cobro de comision: ");
				   //todas las transacciones 500d cobro 5d, lo que este arriba de 500d  se cobrara el 1 porciento
				   if(cantidad > 500)
				   {
					   cobroComision = (cantidad * comisionP)/100d;
				   }
				   else
				   {
					   cobroComision = proveedor.getMin_comision_fija();
				   }
				   
				   bitacora.setCommission(cobroComision);
				   //mandar a realizar el cargo
				   
				   LOGGER.debug("Iniciando peticion Blackston:");
				   //5454 5454 5454 5454 Exp: 08/17
				   BlackstoneRequest Brequest = new BlackstoneRequest();
				   Brequest.setConcepto("comision INGO - Cambio de Cheque");
				   Brequest.setCvv2(UtilsService.getSMS(data.getCt()));
				   Brequest.setCardNumber(UtilsService.getSMS(data.getPan()));
				   Brequest.setExpDate(UtilsService.getSMS(data.getVigencia()).replace("/", ""));
				   Brequest.setIdProducto(1);//Hasta el momento solo hay cambio de cheque
				   Brequest.setIdProveedor(proveedor.getId_proveedor().intValue());
				   Brequest.setIdUsuario(data.getId_usuario());
				   Brequest.setImei(data.getImei());
				   Brequest.setModelo(data.getTipo());
				   Brequest.setMonto(cobroComision);
				   Brequest.setSoftware(data.getSoftware());
				   Brequest.setTipo("M");
				   Brequest.setWkey(data.getWkey());
				   Brequest.setZipCode(data.getUsr_cp());
				   
				   LOGGER.debug("Enviando datos a blackston");
				   BlackstoneResponse BlackRes = mcIngorequest.BlackstonePay(Brequest);
				   LOGGER.debug("reponse: " + BlackRes.getIdBitacora() + " " + BlackRes.getMessage() + " error: " + BlackRes.getErrorCode());
				   bitacora.setIdbitacora(BlackRes.getIdBitacora());
				   
				   if(BlackRes != null && BlackRes.getErrorCode() == 0)
					   bitacora.setStatusCommission(1);
				   else
				   {
					   LOGGER.debug("No se realizo el cobro de comision: " + BlackRes.getIdBitacora() + " " + BlackRes.getMessage() + " error: " + BlackRes.getErrorCode());
					   bitacora.setStatusCommission(0);
				   }
			   }
			   else
				   LOGGER.debug("LA Transaccion no ha finalizado: ");
			   
			   mapper.insertBitacoraIngo(bitacora);
				
				
			}catch(Exception ex){
				LOGGER.error("Error al procesar notificacion:", ex);
				ex.printStackTrace();
				
			}
		}
		
		private void getUser(String user, String pass){
			HashMap<String , String> user_data =  new HashMap<>();
			user_data.put("nusuario", user);
			user_data.put("nclave", pass);
			
			String jsonDataUser = mapper.getUser(user_data);
			LOGGER.debug("JSON: " + jsonDataUser);
			jsonDataUser = jsonDataUser.replace(":,", ":0,");
			User userObject = (User)jsonUtils.jsonToObject(jsonDataUser,User.class);
			LOGGER.debug("ID: " + userObject.getId_usuario());
			LOGGER.debug("Direccion: " + userObject.getUsr_direccion().replace("?",""));
			try {
				byte[] latin1 = userObject.getUsr_direccion().getBytes("ISO-8859-1");
				 
				LOGGER.debug("Direccion: " + new String(latin1));
				
			     
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		 public static String generaSemillaAux(String pass) {
			    int len = pass.length();
			    String key = "";

			    int carry;
			    for (carry = 0; carry < 32 / len; ++carry) {
			      key = key + pass;
			    }

			    for (carry = 0; key.length() < 32; ++carry) {
			      key = key + pass.charAt(carry);
			    }

			    return key;
			  }
		
		
	//decryptSensitive
	//	encryptPublic
}
